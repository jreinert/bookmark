# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bookmark/version'

Gem::Specification.new do |spec|
  spec.name          = "bookmark"
  spec.version       = Bookmark::VERSION
  spec.authors       = ["Joakim Reinert"]
  spec.email         = ["mail@jreinert.com"]
  spec.description   = %q{Makes bookmarking directories in a unix filesystem a breeze}
  spec.summary       = %q{Run bm in your current directory to bookmark it. See -h or --help for options.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_dependency "slugify"
  spec.add_dependency "choice"
end

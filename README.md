# Bookmark

Makes bookmarking directories in a unix file system a breeze

## How it works

Bookmark adds aliases to your shell initialization file

## Installation

`$ gem install bookmark`

## Usage

`$ bm --help`

```
Options:
    -n, --name=NAME                  The name for the bookmark (slugified current directory name is used if not provided)
    -p, --path=PATH                  The path to set the bookmark to (pwd is used if not provided)
    -c, --command COMMAND            Command to execute after entering the bookmarked directory
                                     
        --help                       Show this message
```

### Examples:

- Bookmark current directory:
  `$ bm`

- Bookmark current directory with specific name:
  `$ bm -n some-name`

- Bookmark current directory specify command to run upon entering the directory:
  `$ bm -c 'git status'`

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

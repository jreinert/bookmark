require "bookmark/version"

module Bookmark
  class Bookmark

    def initialize(options)
      @name = options[:name]
      @command = options[:command]
      @path = options[:path]
      @alias = "alias #{@name}='cd #{@path}"
      @alias << " && #{@command}" if @command
      @alias << "'"
    end

    def save!
      File.open(Bookmark.shell_rc, 'a') do |file|
        file.puts @alias
      end
      puts "Bookmark #{@name} saved! Run"
      puts "source #{Bookmark.shell_rc}"
      puts "to start using it."
    end

    def self.shell_rc
      File.join(ENV['HOME'], ".#{File.basename(ENV['SHELL'])}rc")
    end

  end
end
